<?php get_header(); ?>
<section class="site-section py-lg">
	<div class="container">

		<div class="row blog-entries">
			<div class="col-md-12 col-lg-8 main-content">
				<!-- WORDPRESS KODAS -->
				<?php if (have_posts()): while (have_posts()) : the_post(); ?>
						<!-- post title -->
						<h1 class="mb-4"><?php the_title(); ?></h1>
						<!-- /post title -->

						<!-- post details -->
						<div class="post-meta">
							<span class="category">Food</span>
							<span class="mr-2"><?php the_time('F j, Y'); ?> <?php the_time('g:i a'); ?> </span> &bullet;
							<span class="ml-2"><span class="fa fa-comments"></span> <?php //if (comments_open( get_the_ID() ) ) comments_popup_link( __( 'Leave your thoughts', 'html5blank' ), __( '1 Comment', 'html5blank' ), __( '% Comments', 'html5blank' )); ?></span>
						</div>
						<!-- /post details -->

						<div class="post-content-body">
							<?php the_content(); // Dynamic Content ?>
						</div>

						<div class="pt-5">
							<p><?php _e( 'Categories: ', 'balita' ); the_category(', '); // Separated by commas ?>

							<?php the_tags( __( 'Tags: ', 'balita' ), ', ', '<br>'); // Separated by commas with a line break at the end ?></p>
						</div>

						<!-- comments_template();  -->
						<?php comments_template(); ?>
						<div class="pt-5">
							<h3 class="mb-5">6 Comments</h3>
							<ul class="comment-list">
								<li class="comment">
									<div class="vcard">
										<img src="images/person_1.jpg" alt="Image placeholder">
									</div>
									<div class="comment-body">
										<h3>Jean Doe</h3>
										<div class="meta">January 9, 2018 at 2:21pm</div>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur quidem laborum necessitatibus, ipsam impedit vitae autem, eum officia, fugiat saepe enim sapiente iste iure! Quam voluptas earum impedit necessitatibus, nihil?</p>
										<p><a href="#" class="reply">Reply</a></p>
									</div>
								</li>

								<li class="comment">
									<div class="vcard">
										<img src="images/person_1.jpg" alt="Image placeholder">
									</div>
									<div class="comment-body">
										<h3>Jean Doe</h3>
										<div class="meta">January 9, 2018 at 2:21pm</div>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur quidem laborum necessitatibus, ipsam impedit vitae autem, eum officia, fugiat saepe enim sapiente iste iure! Quam voluptas earum impedit necessitatibus, nihil?</p>
										<p><a href="#" class="reply">Reply</a></p>
									</div>

									<ul class="children">
										<li class="comment">
											<div class="vcard">
												<img src="images/person_1.jpg" alt="Image placeholder">
											</div>
											<div class="comment-body">
												<h3>Jean Doe</h3>
												<div class="meta">January 9, 2018 at 2:21pm</div>
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur quidem laborum necessitatibus, ipsam impedit vitae autem, eum officia, fugiat saepe enim sapiente iste iure! Quam voluptas earum impedit necessitatibus, nihil?</p>
												<p><a href="#" class="reply">Reply</a></p>
											</div>


											<ul class="children">
												<li class="comment">
													<div class="vcard">
														<img src="images/person_1.jpg" alt="Image placeholder">
													</div>
													<div class="comment-body">
														<h3>Jean Doe</h3>
														<div class="meta">January 9, 2018 at 2:21pm</div>
														<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur quidem laborum necessitatibus, ipsam impedit vitae autem, eum officia, fugiat saepe enim sapiente iste iure! Quam voluptas earum impedit necessitatibus, nihil?</p>
														<p><a href="#" class="reply">Reply</a></p>
													</div>

														<ul class="children">
															<li class="comment">
																<div class="vcard">
																	<img src="images/person_1.jpg" alt="Image placeholder">
																</div>
																<div class="comment-body">
																	<h3>Jean Doe</h3>
																	<div class="meta">January 9, 2018 at 2:21pm</div>
																	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur quidem laborum necessitatibus, ipsam impedit vitae autem, eum officia, fugiat saepe enim sapiente iste iure! Quam voluptas earum impedit necessitatibus, nihil?</p>
																	<p><a href="#" class="reply">Reply</a></p>
																</div>
															</li>
														</ul>
												</li>
											</ul>
										</li>
									</ul>
								</li>

								<li class="comment">
									<div class="vcard">
										<img src="images/person_1.jpg" alt="Image placeholder">
									</div>
									<div class="comment-body">
										<h3>Jean Doe</h3>
										<div class="meta">January 9, 2018 at 2:21pm</div>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur quidem laborum necessitatibus, ipsam impedit vitae autem, eum officia, fugiat saepe enim sapiente iste iure! Quam voluptas earum impedit necessitatibus, nihil?</p>
										<p><a href="#" class="reply">Reply</a></p>
									</div>
								</li>
							</ul>
							<!-- END comment-list -->

							<div class="comment-form-wrap pt-5">
								<h3 class="mb-5">Leave a comment</h3>
								<form action="#" class="p-5 bg-light">
									<div class="form-group">
										<label for="name">Name *</label>
										<input type="text" class="form-control" id="name">
									</div>
									<div class="form-group">
										<label for="email">Email *</label>
										<input type="email" class="form-control" id="email">
									</div>
									<div class="form-group">
										<label for="website">Website</label>
										<input type="url" class="form-control" id="website">
									</div>

									<div class="form-group">
										<label for="message">Message</label>
										<textarea name="" id="message" cols="30" rows="10" class="form-control"></textarea>
									</div>
									<div class="form-group">
										<input type="submit" value="Post Comment" class="btn btn-primary">
									</div>

								</form>
							</div>
						</div>
					<!-- /article -->

				<?php endwhile; ?>

				<?php else: ?>

					<!-- article -->
					<article>

						<h1><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h1>

					</article>
					<!-- /article -->

				<?php endif; ?>
				<!-- END. WORDPRESS KODAS -->

			</div>

			<!-- END main-content -->

			<?php get_sidebar(); ?>
			<!-- END sidebar -->

		</div>
	</div>
</section>

<?php get_footer(); ?>