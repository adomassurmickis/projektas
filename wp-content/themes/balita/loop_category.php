

<?php if (have_posts()): while (have_posts()) : the_post(); ?>
		<!-- post thumbnail -->
		
	<!-- article -->
	<div class="col-md-6">

                <a href="<?php the_permalink(); ?>">
                
                <div class="image element-animate" data-animate-effect="fadeIn" 
                    <?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
                        style="background-image: url(<?php the_post_thumbnail('cover'); ?>);"
                    <?php endif; ?>
                ></div>
  


                  <div class="blog-content-body">
                    <div class="post-meta">
						<div class="category">Food</div>

                      <span class="mr-2"><?php the_time('F j, Y'); ?> <?php the_time('g:i a'); ?> </span> &bullet;
					  <span class="ml-2"><span class="fa fa-comments"></span> <?php if (comments_open( get_the_ID() ) ) comments_popup_link( 
							__( '0', 'balita' ), __( '1 ', 'balita' ), __( '% Comments', 'balita' )); ?>
							
					</span>
                    </div>
                    <h2><?php the_title(); ?></h2>
                  </div>
                </a>


              </div>
<?php endwhile; ?>
<?php else: ?>

	<!-- article -->
	<article>
		<h2><?php _e( 'Sorry, nothing to display.', 'balita' ); ?></h2>
	</article>
	<!-- /article -->

<?php endif; ?>


