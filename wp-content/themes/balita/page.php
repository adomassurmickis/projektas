<?php get_header(); ?>

	
    <section class="site-section">
      <div class="container">
        <div class="row mb-4">
          <div class="col-md-6">
            <h1><?php the_title(); ?></h1>
          </div>
        </div>
        <div class="row blog-entries">
          <div class="col-md-12 col-lg-8 main-content">
		  <?php if (have_posts()): while (have_posts()) : the_post(); ?>

<?php the_content(); ?>
<?php comments_template( '', true ); // Remove if you don't want comments ?>
	<?php endwhile; ?>
	<?php else: ?>

<?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?>

<?php endif; ?>

          </div>
		  <?php get_sidebar(); ?>
        </div>
     

<?php get_footer(); ?>
