<?php
/*
@version 4.0.0
*/

if(!defined('ABSPATH')) {
    exit;
}

global $post;
?>
<div class="item-details">
	<?php the_content(); ?>
</div>