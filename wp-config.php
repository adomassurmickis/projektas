<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'naujaduombaze' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'mu|$*.zC/XsVPw)=3:v&~)a z#ndv#;v+I~}Cl|*Y:ig/idsdwMAzoma o%Ukh|D' );
define( 'SECURE_AUTH_KEY',  'cv84C|#q@4n/*hh}:Jtk0C8lr{LO)7ZVLwVzFG]0Hl1_Dk{kJOu*F`+@-)3BZN$n' );
define( 'LOGGED_IN_KEY',    '@;!xTn>u@eP#C<ZF7^tQ +C09w+vW(E5UU)_VI#(abuP5w:~i3|=[3j`LNNYoqP{' );
define( 'NONCE_KEY',        'GA0;j$&q5J,?T;=XZ%/y7Ef[TyKue36d-R7FdbXxTP3qefy[=15c7eRZe,Y8uSp=' );
define( 'AUTH_SALT',        '><]xs^fxadYIa{T8u1Y-5RU J##_Z9OJanAX_>gh;wEuR8uS[zVbb$_0`5r_3eyc' );
define( 'SECURE_AUTH_SALT', '.in,w0.}!N-`.(10Hl,};0PP-+`$%_(D0Gj:}O~p;P{*]qPMJ/saFaVz3ODhwt3#' );
define( 'LOGGED_IN_SALT',   'bg`hBUm`Cf+B}-xJV5FG[G~S7QtqfuC_!SdV0Jx <Rh`/G:k a9RzJ8y3IrX-d-:' );
define( 'NONCE_SALT',       'wfJzn>K[Mh`ORBA[_s!v|vY) RMfv21r@9-zu-SA5/p7%Pg&:CHYNJw7!(2w^2yU' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
